﻿using System.Drawing;

namespace Homework12
{
    /// <summary>
    /// Класс отрисовки фона
    /// </summary>
    internal class BackgroundPainter
    {
        public static void DrawBlackBackground(int borderSize, Bitmap image, out Bitmap background)
        {
            background = new Bitmap(image.Width + borderSize * 4, image.Height + borderSize * 4);
            using (var graphics = Graphics.FromImage(background))
            {
                graphics.FillRectangle(Brushes.Black, 0, 0, background.Width, background.Height);
            }
        }
    }
}