﻿using System;
using System.Drawing;

namespace Homework12
{
    /// <summary>
    /// Класс отрисовки рамки
    /// </summary>
    internal class FramePainter
    {
        public static void DrawWhiteFrame(ref Bitmap bitmap)
        {            
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawRectangle(new Pen(Brushes.White, 5), new Rectangle(0, 0, bitmap.Width, bitmap.Height));
            }
        }
    }
}