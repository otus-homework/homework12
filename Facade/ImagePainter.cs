﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework12
{
    /// <summary>
    /// Класс отрисовки изображения
    /// </summary>
    internal class ImagePainter
    {
        public static void DrawImage(Bitmap image, ref Bitmap background)
        {
            using (var graphics = Graphics.FromImage(background))
            {
                graphics.DrawImage(image, background.Width / 2 - image.Width / 2, background.Height / 2 - image.Height / 2, image.Width, image.Height);
            }
        }
    }
}
