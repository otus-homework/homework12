﻿using System;
using System.Drawing;

namespace Homework12
{
    internal class TitleWriter
    {
        internal static void WriteText(string text, int borderSize, ref Bitmap bitmap)
        {
            var sf = new StringFormat()
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center,
            };
            using (var graphics = Graphics.FromImage(bitmap))
            {
                graphics.DrawString(
                    text,
                    new Font("Arial", 14),
                    new SolidBrush(Color.White),
                    bitmap.Width / 2,
                    bitmap.Height - borderSize,
                    sf);
            }
        }
    }
}