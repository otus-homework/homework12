﻿using System;
using System.Drawing;
using System.IO;

namespace Homework12
{
    internal class Motivator
    {
        public Motivator()
        {
        }

        /// <summary>
        /// Cоздание мотиватора
        /// </summary>
        /// <param name="imagePath"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public string CreateMotivator(string imagePath, string text)
        {
            if (!File.Exists(imagePath))
            {
                return $"Ошибка создания мотиватора. Файл с именем {imagePath} не найден.";                
            }

            Bitmap imageBitmap = Read(imagePath);
            int borderSize = 20;

            BackgroundPainter.DrawBlackBackground(borderSize, imageBitmap, out var result);
            FramePainter.DrawWhiteFrame(ref imageBitmap);
            ImagePainter.DrawImage(imageBitmap, ref result);
            TitleWriter.WriteText(text, borderSize, ref result);

            return Write(result);
        }

        /// <summary>
        /// Запись изображения на диск
        /// </summary>
        /// <param name="image"></param>
        private string Write(Bitmap image)
        {
            string path = $@"C:\OTUS\Homework12\Homework12\{DateTime.Now.ToShortDateString()}.jpg";
            image.Save(path);
            return "Мотиватор сохранен по пути: " + path;
        }

        /// <summary>
        /// Считываем файл источник
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Bitmap Read(string source)
        {
            return new Bitmap(source);
        }
    }
}