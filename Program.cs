﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Homework12
{
    class Program
    {
        static void Main(string[] args)
        {
            string imagePath = @"C:\OTUS\Homework12\Homework12\Terminator.jpg";
            string text = "I'll be back";

            Motivator motivator = new Motivator();
            Console.WriteLine(motivator.CreateMotivator(imagePath, text));
            Console.ReadKey();
        }
    }
}
